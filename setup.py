from setuptools import setup, find_packages
import os

setup(
    name='graph_lib',
    version='0.2.0',
    packages=find_packages(),
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
)
