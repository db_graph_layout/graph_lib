# from igraph import Graph as iGraph
import gzip
import random
import struct
import tempfile
from contextlib import closing
from functools import reduce
from itertools import zip_longest

import igraph


class Graph(object):
    __id2method = {
        1: 'circle',
        2: 'drl',
        3: 'fruchterman_reingold',
        4: 'kamada_kawai',
        5: 'lgl',
        6: 'reingold_tilford',
        7: 'reingold_tilford_circular',
    }

    @classmethod
    def __read(cls, method, f, *args, **kwargs):
        graph = method(f, *args, **kwargs)
        return Graph(graph)

    @classmethod
    def get_tmp_file(cls, st):
        f = tempfile.NamedTemporaryFile(dir='/tmp/kursach')
        f.write(st)
        f.seek(0)
        return f

    @classmethod
    def read_graphmlz(cls, f, direction=False):
        return Graph.__read(igraph.Graph.Read_GraphMLz, f, direction)

    @classmethod
    def read_graphml(cls, f, direction=False):
        return Graph.__read(igraph.Graph.Read_GraphML, f, direction)

    @classmethod
    def uncompressed(cls, byte_string):
        graph = igraph.Graph()
        nums = map(lambda x: x[0], struct.iter_unpack('>I', byte_string))
        arr = []
        i = 0
        k = 0
        for n in nums:
            if i == k:
                i = 0
                k = n
                arr.append([])
                continue
            arr[-1].append(n)
            i += 1
        graph.add_vertices(len(arr))
        for i, vx in enumerate(arr):
            graph.add_edges(zip_longest([], vx, fillvalue=i))
        return Graph(graph)

    def __init__(self, graph: igraph.Graph):
        for a in graph.vs.attribute_names():
            del graph.vs[a]
        for a in graph.es.attribute_names():
            del graph.es[a]
        self.graph = graph
        self.__invariant = None
        self.__compressed_graph = None

    @property
    def invariant(self):
        if self.__invariant is None:
            self.__invariant = reduce(bytes.__add__, (map(
                lambda i: i.to_bytes(4, byteorder='big', signed=False),
                (
                    self.graph.vcount(),
                    self.graph.ecount(),
                    *sorted(self.graph.indegree())
                )
            )))
        return self.__invariant

    def isomorphic(self, other):
        return self.graph.isomorphic(other.graph)

    @property
    def compressed_graph(self):
        if self.__compressed_graph is None:
            self.__compressed_graph = gzip.compress(reduce(bytes.__add__, (
                n.to_bytes(4, byteorder='big', signed=False)
                for ind, vs in enumerate(self.graph.get_adjlist())
                for fvs in [list(filter(lambda x: x >= ind, vs))]
                for n in [len(fvs), *fvs]
            )))
        return self.__compressed_graph

    def draw(self, cod):
        method = self.__id2method.get(cod)
        f = tempfile.NamedTemporaryFile(dir='/tmp/kursach', suffix='.svg')

        n = self.graph.vcount()
        k = (n / 200) ** 0.7 if n > 200 else 1
        vs = min(250 / n, 10)
        ew = min(150 / n, 1)
        with closing(f):
            igraph.plot(self.graph.permute_vertices(random.sample(range(n), n)),
                        f.name,
                        layout=method,
                        vertex_size=vs,
                        edge_width=ew,
                        vertex_label=None,
                        edge_color="black")
            f.seek(0)
            svg = f.read()
        return svg.decode()